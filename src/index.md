---
marp: true
theme: gricad
footer: "UAR GRICAD - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---
  
# Decks


[FPMs ANF UST4HPC 2023](./fpm_anf_ust4hpc_2023.html) - [PDF](./fpm_anf_ust4hpc_2023.pdf)
[Café Guix - Gestion des paquets de GRICAD](./cafe_guix_20230407.html)[PDF](./cafe_guix_20230407.pdf)
[GUIX@GRICAD - Workshop NUMPEX - 04/04/2023](./guix_gricad_numpex.html) - [pdf](./guix_gricad_numpex.pdf)
[GUIX@INRAE - 21/03/2023](./guix_inrae_2023.html) - [pdf](./guix_inrae_2023.pdf)
[SO@INRAE - 21/03/2023](./so_inrae_2023.html) - [pdf](./so_inrae_2023.pdf)


---
# GRICAD slides

[GUIX@SARI - 08/12/2022](./sari_guix_2022.html) - [pdf](./sari_guix_2022.pdf)
[GRICAD Énergie - 29/11/2022](./costu_energy.html) - [pdf](./costu_energy.pdf)
[Projets GRICAD- 29/11/2022](./GRICAD_COSTU.html) - [pdf](./GRICAD_COSTU.pdf)
[Cadrage COSTU- 29/11/2022](./costu_cadrage.html) - [pdf](./costu_cadrage.pdf)
[GPUs@GRICAD - 28/02/2022](./GPUs@GRICAD.html) - [pdf](./GPUs@GRICAD.pdf)
[Visite CNRS - 29/03/2022](./GRICAD_CNRS.html) - [pdf](./GRICAD_CNRS.pdf)
[GAIA Data@Grenoble](./gaia_data.html) - [pdf](./gaia_data.pdf)
[GAIAnet](./gaia_mesonet.html) - [pdf](./gaia_mesonet.pdf)
[GRICAD for UGA/McMaster](./gricad_uga_mcmaster.html) - [pdf](./gricad_uga_mcmaster.html)

---
# Mesonet

[GT portail - 04/02/2022](./mesonet_portail_20220204.html)
[COPIL - 04/02/2022](./COPIL_mesonet_20220204.html)
[GT Portail - 13/05/2022](./mesonet_portail_20220513.html)


---
# Reproducibility

[Atelier 1 - FLOSS ESR, 14/01/2022](./atelier_floss_esr.html) - [pdf](./atelier_floss_esr.pdf)