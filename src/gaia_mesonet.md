---
marp: true
theme: gricad
paginate: true
footer: "Le projet Mesonet - 16/05/2022 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---
# Le projet EQUIPEX+ Mesonet
## GAIA Data@Grenoble, le 16/05/2022

*Pierre-Antoine Bouttier, UAR GRICAD, CNRS*

![w:500 center](fig/logo.png)

---
## Le projet Mesonet

- Faciliter l'utilisation des ressources nationales ou régionales 
- **Fournir une infrastructure agile pour le développement des codes informatiques et la formation**
- **Mise en place d'une fédération de stockage**
- Disposer d’infrastructures calcul / ia au meilleur niveau technologique
- Une offre nationale de services associés :
  - mise en place d'un support utilisateur renforcé et partagé  
  - participer à la compétitivité économique, en particulier des PME

---
## Pour résumé

* Un projet fédérateur au niveau des mésocentres de calculs français
* Une machine nationale distribuée et une fédération de stockage 
* À destination de : 
  * L'ESR français (étudiants inclus)
  * Des PME
* Pour : 
  * Les développements de codes, le prototypage
  * L'enseignement et la formation
* Accessibles depuis un portail unique

---
## Schéma global de l'infrastructure Mesonet 

![w:800 center](fig/infra_mesonet.png)

---
## Les partenaires

![w:800 center](fig/part_mesonet.png)

---
## GRICAD dans Mesonet (1/2)

* Représentant du partenaire hébergeur UGA...
* ...groupé avec Lyon 1 au sein de CINAURA
* **Hébergement de la partition openstack**
* **Hébergement d'une brique de la fédération de stockage**

---
## GRICAD dans Mesonet (2/2)

* Copilotage des GTs **Stockage** (B. Bzeznik) et **Portail** (P-A Bouttier)
* Financement d'un IR ASR (pérennisé à l'issue du projet)
* 1 045 000€ pour mise en place de la partition openstack
* 1 403 378€/14 pour la partition stockage

--- 
## Gaia Data et Mesonet

* Possibles points de convergence ?
  * **La fédération de stockage** 
  * Accès, authentification ?
  * Les cas d'usages développement/prototypage

---

# Questions/Discussions