---
marp: true
theme: gricad
paginate: true
footer: "Mesonet GT portail - 04/02/2022  *P-E Guérin, Emmanuel Courcelle, P-A Bouttier*"
---

# Mesonet GT portail - 4 février 2022

*Pierre-Emmanuel Guérin, Emmanuel Courcelle, P-A Bouttier*

---
# Ordre du jour

- **Présentation/Démo IAM par Michel Jouvin** *30min*
- **Définition solution *ad hoc* authentif pour accès aux premières machines**, *1h30*

---
# Solution *ad hoc* d'authentification
 
- Définir les architectures : 
  - Structure du LDAP (quels champs sont nécessaires et suffisants ?)
  - Hébergement
  - Bien intégrer la future migration vers la solution pérenne
  - Commeent rentre-t-on dans ce SI ? (formulaire web, ldapvi ?)
  - Ldap IN2P3 ? Contraintes, avantages ?
    - Retour Cyrille Toulet ? 


---
# Retours

Quelque soit la solution d'authentification choisie, il faut une interface sur les infras pour le provisionnement de compte

Regarder les besoins

Deadline : automne

GRAMC + IAM

Comparatif GRAMC e-dari, portail
GRAMC 

- Programme fonctionnel

- Workflow 

- Remplacement du SI 
---
# Retours 

Faire un document sur le schéma fonctionnel

300k€ 

Diffusions questions 