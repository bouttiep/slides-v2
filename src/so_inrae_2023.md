---
marp: true
theme: gricad
author: Pierre-Antoine Bouttier
paginate: true
footer: SO@INRAE, 22/03/2023 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*
---
<!-- headingDivider: 3 -->

# Science Ouverte et Gestion des Données de la recherche
**Pourquoi et comment ?**
Formation FCC INRAE - 22/03/2023
*Pierre-Antoine Bouttier*, fortement inspiré par [la formation CED de l'UGA - Gestion des données de la recherche](https://scienceouverte.univ-grenoble-alpes.fr/services/formation-et-seminaires/formation-doctorale-gestion-des-donnees-de-la-recherche-2022/)

# La Science Ouverte

**Définition**
> La science ouverte est la diffusion sans entrave des publications et des données de la recherche.

**Objectifs**

> La communication scientifique directe sans délai, sans barrière financière pour permettre la progression de la recherche, les échanges de données et d’information et favoriser leur visibilité par les moteurs de recherche

# Différentes facettes de la Science Ouverte

![w:900 center](./fig/so_hive.png)

# Les piliers

![bg right](./fig/pillars_so.png)

* Les données : Open data
* Les codes : Open Source
* Les publications : Open Access
* Les peer-reviews : Open Peer-Review 

# Enjeux

* Plus de reproductibilité
* Confiance : transparence – éthique – intégrité
* Meilleure diffusion des connaissances
* Pérennité d’accès : accès garanti aux travaux précédents
* Augmenter la durée de vie d’une production scientifique
* Collaborations multidisciplinaires. Permet de nouvelles recherches, de nouvelles coopérations

[**2ème plan national pour la Science Ouverte**](https://www.ouvrirlascience.fr/deuxieme-plan-national-pour-la-science-ouverte/)

# Focus sur les données de la recherche

# Données de la recherche ?

* Données d’observation
* Données expérimentales
* Données de simulation numérique
* Données dérivées ou compilées
* Données de référence
* Données d’enquêtes
* *Publications*
* *Code source*

# Données de la recherche ?

> Ensemble des informations collectées, produites et utilisées par la recherche scientifique

* Des données très variées dans leur forme
* Très dépendantes des disciplines scientifiques
* Ne pas confondre ces données avec les données personnelles ou sensibles au sens du RGPD (bien qu'elles peuvent l'être)

# Le cycle de vie

![center h:500](./fig/datacl.png)

# Le plan de gestion de données (PGD ou DMP)

* **Quelles données vont être obtenues** : quels types de données, comment sont-elles collectées, où les stocker, sécurisation, documentation, format, organisation, etc.
* **Comment vont-elles être utilisées** : comment on les partage, comment on les traite, où on les traite, ...
* **Comment elles vont être préservées** : à quel terme, quelles données, où, comment ...
* **Comment elles vont être valorisées** : comment les diffuser, sous quel format, sous quelle licence, quelles données, comment associer les codes, ...
* **Comment assurer le financement des ressources nécessaires ?**

# Le Plan de gestion des données

* Nous aide à nous poser des questions...
* ...pour faire les choix les plus pertinents possibles
  * Pratiques
  * Outils
  * Infrastructures
* **C'est un document vivant, amené à évoluer tout au long de l'étude scientifique**, tout comme les réponses aux questions sur les données...


# Contexte de la Science Ouverte

![center w:700](./fig/fair.jpg)

Les réponses aux questions précédentes doivent prendre en compte les **principes FAIR** de la Donnée Ouverte.

Par défaut, l’ouverture des données de la recherche est obligatoire selon le principe :
> Aussi ouvert que possible, aussi fermé que nécessaire

# Les exceptions

* Données personnelles non anonymisées ou pseudonymisées
* Données non achevées (sauf données géographiques qui peuvent être inachevées,
directive INSPIRE), certaines données environnementales (cf protection des espèces)
* Photos quand il y a une personne reconnaissable
* Données scrapées (car souvent interdit par les conditions d’utilisation) sauf :
si le préjudice du producteur est nul (risque encouru nul également) le scraping d’une partie non substantielle des données reste possible

# Les enjeux pour le travail de recherche ?

* Assurer l’exploitation des données tout au long du projet
* Assurer la conservation des données à moyen terme
* Pouvoir reproduire les résultats scientifiques, y compris par l’équipe
qui les a obtenus !
* Pouvoir facilement les réutiliser pour produire de nouvelles
recherches
* Valoriser les résultats scientifiques, mais aussi les données et les
codes qui ont permis de les obtenir et en augmenter la visibilité
* Favoriser de nouvelles collaborations, de nouvelles approches


# Les enjeux d’ordre sociétaux

* Assurer une souveraineté sur les données produites
* Assurer l’intégrité scientifique
* Garantir la transparence, et assurer la confiance des citoyens en la
recherche

# Mais comment diable fait-on tout ça ? 

De la même manière que pour la reproductibilité, les principes FAIR sont un idéal à atteindre.

* Ensemble de bonnes pratiques...
* ... dont vous appliquez déjà certaines...
* ... qui peuvent être soutenues par des outils et des infrastructures

# L'organisation des données - Quelques notions

# ![center h:600](./fig/filenames.png)

# Pourquoi l’organisation de ses données est importante ?

* Avez vous déjà ouvert un fichier sans comprendre ce qu’il y avait dedans ? 
* Avez vous déjà travaillé sur la mauvaise version d’un fichier ?
* Avez-vous déjà écrasé un fichier ?
* Avez vous déjà été dans l’impossibilité de retrouver un fichier ?
* Avez vous déjà été confronté au fait de ne plus pouvoir ouvrir un fichier ?

# Pourquoi l’organisation de ses données est importante ?

- Avez vous déjà ouvert un fichier sans comprendre ce qu’il y avait dedans ? 
- Avez vous déjà travaillé sur la mauvaise version d’un fichier ?
- Avez-vous déjà écrasé un fichier ?
- Avez vous déjà été dans l’impossibilité de retrouver un fichier ?
- Avez vous déjà été confronté au fait de ne plus pouvoir ouvrir un fichier ?

![center h:200](./fig/ouioui.jpg)

# Les points importants : FAIR pour soi-même

**L’organisation des données**
Être en capacité de retrouver facilement et rapidement n’importe quel contenu : Findable / Accessible

**Les formats de fichiers utilisés**
Être en mesure d’ouvrir ses fichiers même si on change de système ou plusieurs années après : Accessible / Interoperable

**Le nommage des fichiers**
Être capable d’identifier rapidement le contenu d’un fichier : Interoperable / Reusable

# Réfléchir à l’organisation de ses données

* **Prendre du temps pour en gagner ensuite**
* Etre pragmatique et réaliste : faire ce qu’il faut sans en faire trop !
* Pas de règle : la bonne organisation est celle qui vous convient. La perfection n’existe pas !
* Mais il faut prendre en compte le contexte : qui va utiliser les données ? Vous seul, l’ensemble de l’équipe, tous les partenaires du projet ...
* N’oubliez pas de décrire l’organisation que vous aurez choisie, même si ce n’est que pour vous même !

# Réfléchir à l’organisation de ses données 

En résumé, les différentes étapes :
➔ Déterminer la structure de l’organisation
➔ Définir la convention de nommage
➔ Spécifier la façon de gérer les versions
➔ Identifier les formats de fichiers des différents types de données

# Réfléchir à l’organisation de ses données 

En résumé, les différentes étapes :
**➔ Déterminer la structure de l’organisation**
➔ Définir la convention de nommage
➔ Spécifier la façon de gérer les versions
➔ Identifier les formats de fichiers des différents types de données

# Organisation hiérarchique

- Mettre en place une arborescence à base de répertoires et sous-répertoires en regroupant les données logiquement
- Ne pas hésiter à mettre un fichier Readme dans chaque répertoire pour expliquer le type de données qui s’y trouvent
- Eviter une trop grande profondeur

# Réfléchir à l’organisation de ses données 

En résumé, les différentes étapes :
➔ Déterminer la structure de l’organisation
**➔ Définir la convention de nommage**
➔ Spécifier la façon de gérer les versions
➔ Identifier les formats de fichiers des différents types de données

# Nommer les répertoires et fichiers

De façon à ce qu’on en identifie directement le contenu. Par exemple pour les données d’un projet de recherche :
* Nom ou acronyme du projet (identifiant unique)
* Nom ou initiales du chercheur
* Date de la constitution des données
* Type de données
* Conditions de collecte
**Et toujours construit de la même manière !**
Les informations dans le même ordre, la plus importante en premier, penser qu’on peut vouloir traiter automatiquement les fichiers (scripts, codes) et documenter la construction des noms des répertoires et fichiers

# Bonnes pratiques de nommage

- Ne pas répéter les informations déjà contenues dans les noms de répertoire
- Des noms les plus courts possibles
- Utiliser la langue la plus adaptée à l’équipe de recherche
- Pas d’espace ni de caractère spécial (y compris éviter les accents, cédille ...) : utiliser des majuscules pour séparer les mots ou des « _ » (underscore)
- Dates au format AAAMMJJ (facilite l’affichage par ordre chronologique) 
- Chiffres à écrire avec le nombre de caractères significatifs :
  - Pour une séquence de 1 à 10 : 01-10 
  - Pour une séquence de 1 à 100 : 001-100

# Réfléchir à l’organisation de ses données 

En résumé, les différentes étapes :
➔ Déterminer la structure de l’organisation
➔ Définir la convention de nommage
**➔ Spécifier la façon de gérer les versions**
➔ Identifier les formats de fichiers des différents types de données

# La gestion de version

Certains documents peuvent être amenés à évoluer, à avoir plusieurs versions avant une version finale, définitive.

Deux solutions :
* Préciser la version dans le nom du fichier
* Utiliser un gestionnaire de version (important quand plusieurs personnes sont susceptibles de modifier les fichiers)

# La gestion de version

Pourquoi ?

* Pouvoir rétablir une version antérieure en cas d’erreur
* Pouvoir accéder à l’historique des modifications et donc de la vie du fichier 
* Pouvoir identifier les auteurs des modifications (utilisation d’un gestionnaire de version)
* Eviter de travailler sur une version périmée du fichier

# La gestion de version

Comment ? 

* Via le nommage des fichiers :
  * Nécessite une discipline de fer
  * Possible si tout petit nombre de fichiers, qui évoluent peu, modifiés par une seule personne
  * Déconseillé

# La gestion de version

Comment ? 

- Via le nommage des fichiers
- Via une gestion de version implémentée dans un logiciel d'édition des données
- Un gestionnaire de version
  * Pour les fichiers texte brut : Git, mercurial
  * Pour les données plus volumineuses : `git-annex`
  * Faits pour ça
  * Demande un temps d'apprentissage

# Réfléchir à l’organisation de ses données 

En résumé, les différentes étapes :
➔ Déterminer la structure de l’organisation
➔ Définir la convention de nommage
➔ Spécifier la façon de gérer les versions
**➔ Identifier les formats de fichiers des différents types de données**

# Les formats de fichiers

- Format = manière dont les données sont structurées, organisées et encodées sur le support physique. Mode de représentation et de stockage des données.
- En général, défini par l’extension dans le nom du fichier
- .mp3, .doc, .fst, .nc, .png, .jpg, etc.

# Pourquoi c'est important ? 

- Etre capable d’ouvrir les fichiers sur un maximum de types de système avec un maximum de logiciels
* Etre capable de le faire maintenant et dans 5 ans
* Pouvoir partager ses données au sein de son équipe de recherche ou de façon plus large
* Pouvoir traiter et analyser les données grâce à différents logiciels, être capable de croiser des données de différentes sources, et donc différents fichiers, éventuellement avec des formats distincts
* **Assurer l’interopérabilité des données**

# Choisir un standard existant en priorité

![center w:900](./fig/standards_2x.png)

# Format ouvert vs fermé 

- Format ouvert = mode de représentation (spécifications techniques) rendu public par son auteur et aucune entrave légale ne s’oppose à sa libre utilisation (droit d’auteur, brevet, copyright).
- Standard ouvert = format ouvert ou libre qui a été approuvé par une organisation internationale de standardisation.


> Les données ont de la valeur : ne pas se faire piéger par un format fermé qui contraindra la liberté de choix quant aux programmes, libres comme propriétaires, qu’on voudra utiliser.

# Format ouvert vs fermé

- Règle 1 : utiliser des formats ouverts
- Règle 1bis : utiliser des formats ouverts
- Règle 2 : utiliser les formats les plus fréquents dans votre communauté
- Beaucoup de liste de formats ouverts sur le web
  - Un site intéressant : https://facile.cines.fr/
  - outil qui permet de vérifier si un fichier est valide et bien formé par rapport au format déclaré, et donc de savoir s’il est éligible à l’archivage proposé par le CINES.
- Lien entre format de données et préservation numérique à la BnF : https://www.bnf.fr/fr/formats-de-donnees-pour-la-preservation-numerique

# Vous savez maintenant organiser vos données dans le contexte de la science 
*À peu près*

# Ce que je n'ai pas évoqué - Gestion des données de la recherche

- Le stockage des données et leur sécurisation
- Le cas des données personnelles et sensibles
- La description des données
- La diffusion des données (entrepôts, DOI)
- L'archivage
- ...

**Il faut pourtant avoir conscience des bonnes pratiques pour tous ces aspects pour tendre vers de la donnée ouverte. Le PGD permet de se poser les questions !**  

# Ce que je n'ai pas évoqué - La Science Ouverte

Pour la Science Ouverte, nous avons effleuré l'Open Data. Restent : 
- Le cas des publications
- Le cas des codes sources
- Le cas des revues par les pairs.

Là, encore, tout est histoire de pratiques et d'outils qui vous aident à les appliquer. 

Vous en appliquez sûrement certaines, sans forcément le savoir.   

# Comment faire mieux ? 

* Formez-vous sur la gestion des données, l'Open Access
* Si vous écrivez du code source, formez-vous aux bonnes pratiques 
* Si vous manipulez des environnements logiciels, utilisez, dès que c'est possible, des outils appropriés (GUIX 😄)

# Merci de votre attention ! 