---
marp: true
theme: gricad
paginate: true
footer: "GPUs@GRICAD - 28/02/2022 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---

# L'utilisation des GPUs à GRICAD (et ailleurs)

*Pierre-Antoine Bouttier, UAR GRICAD, CNRS*


![w:500 center](fig/logo.png)

---
# La trajectoire (très) générale 

* Jusqu'à ces dernières années, GPU utilisés pour l'accélération de codes de calcul : K80M sur Froggy depuis 2014. 
* Avènement de l'IA 
  * Explosion (en plsuieurs temps) de la demande GPU
  * Appel d'air pour l'accélération des codes de calcul (vers l'exascale)

---

# Les moyens 

--- 
# L'offre nationale 

* **IDRIS :  Supercalculateur Jean Zay**
  * 2 partitions GPUs (A100 et V100)
* TGCC : Supercalculateur Joliot-Curie/Irene
  * 1 partition GPU (V100)
* Accès : 
  * Demande Classique e-dari 
  * [Pour utilisation <50kh.GPU, accès dynamiques (pour l'ensemble des calculateurs)](https://www.genci.fr/fr/node/1167)

---
# L'offre GRICAD

* HPC Très bientôt (lundi prochain) : 
  * Bigfoot, cluster dédié GPU : 
    * 8 noeuds quadri-GPUs NVIDIA V100, reliés en NVLINK? 192Gà de mémoire
    * 5 noeuds avec bi-GPUs NVIDIA A100 (1 en partage MIG, actuellement)
    * Focus IA (en lien avec `/silenus`)
    * Env logiciel : géré actuellement en production par `conda`
  * NOVA (VM à la demande) :
    * 2 noeuds bi-GPUs A100≈32vGPUs
    * Actuellement en cours d'achat de licences

---
# À venir

* Niveau national :
  * Extensions planifiées (e.g. 52 noeuds octo-GPUs sur Jean Zay, 2022)
  * Infrastructure Mesonet : partition GPU HPC et GPU openstack
* Niveau GRICAD : 
  * ajustement de l'offre en fonction de la demande (et des financements) 
  * Prévision d'achat de GPUs AMD, sur budget OSUG

--- 

# L'accompagnement

* Support classique : accès et utilisation des ressources, env logiciels
* Collaboration GRICAD/chercheurs sur projets ciblés : en fonction de nos ressources (expertise, dév adhoc, co-encadrement)
* PNRIA : 
  * ingénieurs experts IA sur projets ciblés et sélectionnés
  * Ouverture ? 

---
# Les scénarios envisageables : 

* Utilisation des méthodos IA dans de nombreux champs de recherche : demande GPU croissante pour cet usage
* Futur horizon du HPC : Exascale => passage obligé par le GPU. Dev/Portage des codes de simu ?
* Attention aux autres technos émergentes : ARM, Vectoriel qui revient, quantique... 
* **Frein énergétique** : coût de l'électricté qui commence à devenir critique 



