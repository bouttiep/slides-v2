---
marp: true
theme: gricad
paginate: true
footer: "COSTU@GRICAD - 29/11/2022 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---

# Cadrage du COmité Scientifique et Technique des Utilisateurs GRICAD

*Pierre-Antoine Bouttier, UAR GRICAD, CNRS*


![w:500 center](fig/logo.png)

---
# Missions du COSTU

* **Relayer les questions/informations entre les communautés scientifiques et GRICAD**
* Assistance à la direction de GRICAD pour le pilotage en ce qui concerne : 
  * Les prospectives en terme de projets à venir, d'acquisition et de déploiement de matériel
  * Les évolutions des modalités d'exploitation des services et infrastructures GRICAD
  * La réflexion concernant d'éventuelles mises en place de nouveaux services

---
# Les canaux de communication

* [Une mailing-list](gricad-costu@univ-grenobles-alpes.fr)
  * N'hésitez pas à vous en servir ! (nous n'hésiterons pas de notre côté)
* Idéalement, 3 réunions par an (pas forcément en présentiel)

---

# Merci d'avoir accepté de participer !