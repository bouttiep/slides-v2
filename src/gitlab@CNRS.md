---
marp: true
theme: socrates
author: Pierre-Antoine Bouttier
paginate: true
footer: GitLab@CNRS, 29/06/2023 - pierre-antoine.bouttier@univ-grenoble-alpes.fr
---

<!-- _class: titlepage -->


![bg left:33% fit](fig/jupgiter.png)
# Notebooks Jupyter et GitLab
## Retour d'expérience

### GitLab@CNRS - 29/06/2023
#### [Pierre-Antoine Bouttier](mailto:pierre-antoine.bouttier@univ-grenoble-alpes.fr)

---
# D'où je parle

* Ingénieur de recherche, expert en calcul scientifique\* spécialisé en mathématiques appliquées, **responsable d'équipe support/accompagnement**...
* ...travaillant à **l'UAR GRICAD**, basée à Grenoble, fournissant **services, infrastructures et expertise** en soutien à toutes les communautés de recherche grenobloises autour du **calcul scientifique**, du **développement logiciel** et de la **gestion des données de la recherche**.  
* Je ne suis pas...
  * ...un ASR
  * ...un chercheur en informatique

> \* Anciennement ?
---