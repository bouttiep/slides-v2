---
marp: true
theme: gricad
paginate: true
footer: "Mesonet GT portail - 13/05/2022  *P-E Guérin, Emmanuel Courcelle, P-A Bouttier*"
---

# Mesonet GT portail - 13 mai 2022

*Pierre-Emmanuel Guérin, Emmanuel Courcelle, P-A Bouttier*

--- 
## OdJ


- IAM : Avancées ? (pas de tests openstack, pour le moment)
- Cahier des charges *Mesoportail*
  - [Lien vers document de travail](https://box.in2p3.fr/index.php/f/22430452)
  - Présentation de l'avancement et question en suspens

---
## Cahier des charges - dans le périmètre 

–	**SI Recherche et enseignement** : descriptifs des projets et de membres => il convient de définir ce que l’on souhaite comme information pour les deux entités
–	**Réservation** : réservation de ressources pour une séance (de cours par exemple)
–	**Allocations** : Procédure d’allocation de ressources.
–	**Usage** : vision live et sur une période de temps de qui a fait quoi sur les machines et stockage
–	**Rapport d’activité** : Recueil de données permettant d’éditer un rapport d’activité annuel pour l’ANT

---
## Cahier des charges - hors du périmètre

–	Documentation : Site web ou wiki permettant de mettre en ligne la documentation utilisateurs
–	Site web : Site web du projet
–	Support : système de tickets pour les demandes de support
–	Autres outils : lancer des notebook, des sessions de visu, des containers, lien avec openData/OpenScience, ...

--- 
## Cahier des charges - Quelques hypothèses retenues

- Accès aux ressources mesonet conditionnées par compte utilisateur actif et appartenance à uun projet actif (validé)
- 2 types d'attributions :
  - Accès dynamiques
  - Accès réguliers
- Ressources : 
  - Accès aux machines, quota d'heures et de stockage
  - Accès à l'infra stockage (Accès sans heures de calcul ?)
  - Logiciels sous licence ? 
  - ? 
- Référentiel edugain (comptes CRU pour extérieurs, e.g. entreprise)
- Quid des projets enseignements ? 

---
## Questions au GT - Attribution de ressources

- Comment étudie-t-on les demandes créations de projet ? 
  - Mission du/des experts/comités d'attribution ? 
    - attribution de ressources ou autorisation ?
    - Orientation du demandeur vers une machine ?
    - Pour une version de projet fait-on appel à un ou à plusieurs experts ?
    - Experts en comité pour un (sous-)ensemble de projets ou projets par projets ?
    - Différence accès réguliers/accès dynamiques ?

---
## Questions au GT - Comptes & projets

- Faut-il faire signer 
- Comptes étudiants ? Fusion de comptes
- Faut-il imposer le même nom d’utilisateur sur toutes les machines de mesonet ?
-	Faut-il imposer des numéros uid-gid sur toutes les machines de mesonet ?
-	Faut-il imposer des noms de groupes ?
-	Les projets dynamiques ont-ils le même « espace de nommage » ?

*Cf. Présentation workflow du dari ?*
