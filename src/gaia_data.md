---
marp: true
theme: gricad
paginate: true
footer: "GAIA data@Grenoble - 16/05/2022 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---
# Présentation de GRICAD
## GAIA Data@Grenoble, le 16/05/2022

*Pierre-Antoine Bouttier, UAR GRICAD, CNRS*

![w:500 center](fig/logo.png)

---
# GRICAD dans les (très) grandes lignes

## Les missions principales

* **Accompagnement, conseils et formations** aux chercheurs sur leurs besoins liés au calcul (au sens large) et aux données
* Mise à disposition à l’ensemble des chercheurs et personnels en support de la recherche d'**infrastructures avancées et mutualisées** pour le calcul (intensif) et l’exploitation des données de la recherche.

---
# L'organisation de l'Unité d'Appui à la Recherche (UAR)

![width:800px center](fig/organigramme_mai2022.png)

---
# Les infrastructures de GRICAD

![w:800 center](fig/infras.png)

---
# CIMENT@GRICAD : les supercalculateurs

---
## Dahu, le cluster principal (1/2)

- Caractéristiques :  
  * 71 noeuds CPU, tout usage (2 CPUs, 32 coeurs, 192Go RAM)
  * 35 noeuds CPU réservés calcul distribué
  * 7 noeuds fast (moins de coeur, fréquence CPU plus élevée)
  * 4 noeuds fat (16 coeurs, 1.5 To de RAM)
  * 1 noeud de visu

---
## Dahu, le cluster principal (2/2)

* Environnements logiciels
  * Module (déprécié)
  * Nix, Guix
  * Conteneurs : singularity
* Une frontale dédiée workflow (`dahu-workflow1`)
* Usage en fair-sharing uniquement
* Machine chargée...
  * Une extension à venir
  * mais problématique d'hébergement

---
## Bigfoot, le cluster GPU (1/2)

* 7 noeuds 4 GPUs V100 (dont 4 NVLINK), 192Go RAM
* 5 noeuds 2 GPUs A100, 192Go RAM
* 35 noeuds T4

---
## Bigfoot, le cluster GPU (2/2)

* Environnement logiciel
  * Nix/Guix mais encore expérimental dans certains cas d'usage (driver nvidia, frameworks IA compliqués à packager from scratch...). Même env que Dahu. 
  * Conda
  * Conteneurs : singularity
* Raisonnablement chargé

---
# CIMENT@GRICAD : les espaces de stockage

---
## *Bettik*, espace de stockage haute performance (≃WORKDIR)

* Espace commun Bigfoot, Dahu (et luke)
* Système BeeGFS
* 1,3To occupés à ≃ 80% ; quota/user 7To, extensible sur demande

---
## *Silenus*, scratch partagé haute performance

* Accessible depuis noeuds OPA de dahu et bigfoot
* Système BeeGFS
* **Temps d’accès les plus faibles possibles et des débits les plus élevés possibles**
* 70To utiles
* Effacement automatique des données inutilisées depuis 30 jours 

---
## MANTIS2, la zone de stockage tiède/d'échange

* Système iRods
* 2Po + 1,5Po redondés 
* Quota/user : 1To (largement) extensible sur demande

---
# NOVA@GRICAD, la plateforme de Cloud Computing

---
## NOVA, la plateforme de cloud computing

* Plateforme Openstack, stockage CEPH
* 14 noeuds compute dont 2 noeuds bi-GPUs (≃32vGPUs)
* 8 noeuds de stockage (570To)
* VM à la demande, différents gabarits par défaut (Ressources, OS)

---
# Services et accès aux ressources

---
## L'accès aux ressources de calcul intensif

* Qui peut accéder aux ressources de calcul intensif ?
  * Recherche académique grenobloise et collaborateurs
* Compte actif et projet actif : accès aux machines (SSH)
  * Seuls les permanents peuvent créer des projets
  * Review des projets et rapports annuels pour prolongation
  * Fair-sharing

---
# Les autres services

- Plateforme BinderHub (partage de notebook), en version beta
- Plateforme JupyterHub
- Gitlab

---
# Le modèle économique

- Sources de financement actuelles
  - **Tutelles**
  - **Soutien financier des labos, projets scientifiques** : 
    - Achat direct d'équipement
    - Achat de services
  - Appels d'offre spécifiques auxquels répond GRICAD : *e.g.* **CPER**, PIA3, etc.

---

# Questions/Discussion/Démo eventuelle