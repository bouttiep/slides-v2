---
marp: true
theme: gricad
paginate: true
footer: "COSTU@GRICAD - 29/11/2022 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---

# Implication de GRICAD dans les *projets scientifiques et techniques*

*Pierre-Antoine Bouttier, UAR GRICAD, CNRS*


![w:500 center](fig/logo.png)

---
# Typologie des projets

GRICAD est impliqué formellement, **de diverses manières**, dans plusieurs types de projets (qui peuvent se recouvrir) : 
- **Développements calcul/données** dans des projets **scientifiques** (centrés sur la production de connaissance)
- **Expertise et mise à disposition d'infras et services** pour des **projets scientifiques avec une dimension fédératrice** pour les communautés de recherche
- **Coordination & mise en place d'infrastructures et services** dans des projets **équipements** 

---
# Intérêts vitaux pour GRICAD

Ces participations nourrissent fondamentalement nos activités : 
- Vitales pour **l'évolution de nos infrastructures et de notre offre de services**
  - Recueil des besoins, au plus près des communautés de recherche
  - Sources de financement  
- **Positionnement dans les bons périmètres** :
  - Locaux, régionaux, nationaux
  - Liens avec les communautés scientifiques
  - Visibilité
- Mais très chronophages

---
# Développements calcul/données pour les projets scientifiques (1/2)

* **ERC F-IMAGE** : Deep Learning sur données sismiques *(ISTerre/MIT - Académie des Sciences)*
* **ERC MONIFAULT** : Template matching pour création de nouveaux catalogues *(ISTerre/Los Alamos Lab/INGV)*
* **ANR Day-stress** : Lien entre stress et les performances motrices - Responsable WP calcul/getsion de données *(CNRS/UGA/CEA co-encadrement Post-Doc)*

---
# Développements calcul/données pour les projets scientifiques (1/2)

* **CASCARA** : Prédire et mesurer l'aggravation de la scoliose *(Pr Courvoisier chirurgien Clinique/CHU)*
* **Etude de la fatigue/blessure** : Modélisation du geste de compensation lors d’un état de fatigue ou de blessure *(AMU - TIMC - GRICAD - GDR Sport)*

---
# Développements calcul/données pour les projets scientifiques - Cas particulier PNRIA

* Dans le cadre du PNRIA, IRs recrutés pour :
  - Développement logiciel et/ou méthodes numériques pour l'IA...
  - ...en soutien à des projets de recherche sélectionnés...
  - ...dans différents centres dont GRICAD
* Encadrement de 3 IRs qui sont chacun sur 2/3 projets (locaux et distants)

---
# Projets fédérateurs - PIA3 PEGASE

- **Objectif** : Création de pôles pilotes de formation des enseignants et de recherche pour l’éducation
- **Partenaires** : UGA, les INSPE et rectorats des Académies de Grenoble et de Guyane, l’Université Savoie Mont Blanc (USMB)
- **Pour GRICAD** : 
  - accompagnement PGD et conseil sur la politique de gestion des données (y compris sensible) globale au projet
  - accompagnement diffusion des données 

---
# Projets fédérateurs - PIA3 TIRREX

- **Objectif** : développement de nouvelles plateformes en robotique avec une coordination nationale pour leur accès et leur développement.
- **Partenaires** : CNRS, INRIA, CEA, INRAE, Universités.
- **Pour GRICAD** : 
  - accompagnement PGD et conseil sur la politique de gestion des données globale au projet
  - accompagnement diffusion des données (sur un entrepôt encore à définir) 

---
# Projets fédérateurs - PIA3 Gaia Data

- **Objectif du projet** : développer et mettre en œuvre une **plateforme intégrée et distribuée de services/données** pour l’observation, la modélisation et la compréhension du système Terre, de la biodiversité et de l’Environnement. 
- **Partenaires** : CNRS (coordinateur), CNES, IFREMER, IRD, BRGM, IGN, INRAE, MétéoFrance, MNHN, CEA, IPGP, CINES, Sorbonne Univ., Univ. Grenoble-Alpes, Univ. Lille, Univ. F. Toulouse, UNISTRA, SHOM, OCA, FRB, CERFACS.
- **Pour GRICAD** : noeud grenoblois de la plateforme

---
# Projets fédérateurs - PEPR DIADEME

- **Objectif** : développer des matériaux innovants, performants, durables et issus de matières premières non critiques et non toxiques
  - Mise en place de plateforme de gestion et de traitement de données fédératrices pour la communauté "matériaux" française
- **Partenaires** : CNRS, CEA, U. Paris-Saclay, Sorbonne U., Institut Polytechnique de Paris, U. Grenoble-Alpes, U. de Lorraine, U. de Bordeaux, U. de Lyon.
- **GRICAD** : l'un des deux centres de calcul et gestion de données retenus pour le développement de la plateforme, avec le TGCC.

---
# Projets équipements calcul & données - PIA3 Mesonet

- **Objectif** : mise en place d'une infrastructure nationale et distribuée de calcul et de gestion de données pour la formation et le développement, à destination de l'ensemble de l'ESR
- **Partenaires** : GENCI, 22 partenaires (Universités, observatoire, mésocentres, grandes écoles
- **GRICAD** (pour l'UGA) : partenaire hébergeur, coordinateur pour la mise en place du portail du projet. 

---
# Projets équipements calcul & données - consortium CINAURA

- **Objectifs** : construire la convergence de l'offre régionale d'infrastructures et de services pour le calcul scientifique et la gestion des données de la recherche
- **Partenaires** :  Grenoble, Lyon, Clermont, Savoie-Mont Blanc 
- **Pour l'UGA et GRICAD** : coordination à l'échelle régionale

---
# Remarques conclusives (1/2)

* GRICAD est également impliquée dans 3 projets locaux au niveau de l'UGA : CDP Sports & Santé, CDTools OTE et MyWay2Health
* La présence au sein de ces projets aux objectifs et périmètres divers nous permet également de porter des sujets transverses d'importance pour nos tutelles et nous : 
  * Science Ouverte (Données FAIR, reproductibilité logicielle)
  * Impacts environnementaux des activités numériques de la recherche
* Dans la quasi-totalité des projets, forte imbrication des problématiques **données ET calcul**

---
# Remarques conclusives (2/2)

* Redondance de besoin de plateformes calcul/données distribuées : 
  * Travail sur l'industrialisation de plateformes identifiées (partage/traitement de gros volumes de données)
  * GRICAD, relai entre les initiatives (ex. : Mesonet/Gaia Data)
* Des recrutements (CDD) qui découlent de notre participation à ces projets : 
  * Renforcement *partiel* des équipes GRICAD
  * Charge de travail et d'organisation (administratif, encadrement) supplémentaire