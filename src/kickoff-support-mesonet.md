---
marp: true
theme: gricad
paginate: true
footer: "Kickoff GT Support Mesonet - 06/02/2023 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---
# Kickoff GT Support MesoNET

*06/02/2023*


---
# OdJ

- Tour de table
- Discussions autour des outils essentiels du support : 
	- Documentation
	- Gestionnaire de demande de support (ticketing)
- (Début de) définition du périmètre et des missions du support mutualisé MesoNET

---
# Tour de table

- Qui sommes-nous ? 
- Comment voyez-vous le support dans MesoNET ? (périmètre, organisation)
* *Appel à volontaire pour la prise de notes ?*

Bien séparer : 
* Généraliste : tout le monde peut répondre
* Question spécifique : là, plus précis, pas forcément tout le monde

Niveau de support différents

Dépanner les machines : partage d'expertise, recette, routine

Outils : whatsapp, visio, lien + direct, slack

Installation de codes : ne pas refaire le travail

PB différentes sur archi spécialisées et code formation

Monitoring : Etat des lieux de l'ensemble des infras 
Etat du service

---
# Documentation

- Outil
  - CMS ? (e.g. wordpress, Drupal ?)
  - Site statique (Jekyll, Hugo, etc.) - Démo possible sur Doc GRICAD
  - Doc publique ou authentification ?
- Organisation de la doc, à court et moyen terme 
  - Par machine ? 
  - Par usage ? 
  - Un peu les deux ? 
- Qui contribue ? Comment contribuer ? Ligne éditoriale
- Hébergement, ndd ? 

---
# Gestionnaire de tickets 

- Démo Request Tracker : http://sos-mesonet.univ-grenoble-alpes.fr/
- Pour l'instant, j'administre l'instance tout seul, et pas de connexion à Indigo IAM et pas d'envoi de mail
- Appel à volontaire pour tester (via l'interface en ligne) 
  - Rôle utilisateur 
  - Rôle intervenant 
  - Il me faut mail, nom, prénom et rôle souhaité
  - Test jusqu'à la prochaine réunion ? 

---
# Missions, périmètre et organisation

* Missions : soutien (support direct et écriture de docuemntation) aux utilisateurs de 
  * Portail (authentif, gestion de ressources)
  * Machines calcul (archi. spéc., code/formation) : connexion, lancement de jobs, envs logiciels ? 
  * Fédération de stockage
  * Bonne exploitation des ressources (msie en place et vérification)

* Supervision : 
  * Monitoring : 
    * GT dédié ? Monitoring, supervision, reporting
    * Météo des services 


* Organisation : support mutualisé
  * Qui dedans ? 
  * Lien avec les ASR ? 
  * Quelle "gouvernance" ? 
  * ? 

---
# TODO et Prochaine réunion