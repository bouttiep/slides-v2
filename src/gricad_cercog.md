---
marp: true
theme: gricad
paginate: true
footer: "CerCog@UGA - 21/06/2022 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---
# GRICAD: data and computing services for research in Grenoble
## CerCog@UGA, June 29, 2022

*Pierre-Antoine Bouttier, UAR GRICAD, CNRS*

![w:700 center](fig/logo.png)

---
## What is computing? What about data? 

![w:700 center](fig/gricad_context.png)

---
## The example of GRICAD (1/3)

* UAR: Unité d'Appui à la Recherche. Structure at the service of research labs.
  * **Support and advice** to researchers on their computation, software development and data needs. 
  * Provision of advanced and shared infrastructures for intensive computing, cloud computing, AI, and the research data exploitation...
  * ...For all researchers, research support staff and their collaborators

---
## The example of GRICAD (2/3)

* **Pooling and rationalisation** of material and human resources within the Grenoble Site (UGA, CNRS, INRIA, G-INP)
* What we don't do (so far): support for teaching only from master level upwards). However, lots of collaborations with the UGA information systems department (DGDSI)

---
## The example of GRICAD (3/3)

![w:700 center](fig/organigramme_mai2022.png)

---
## The GRICAD service offer

---
## GRICAD services

* Counseling and support
* Computing Center (HTC, HPC? data analysis)
* Cloud computing 
* Data management, design and engineering
* Gitlab
* Training and animation
* Audiovisual production

---
## GRICAD access policy

GRICAD services are freely accessible to all academic researchers belonging to the Grenoble research site and to all their collaborators within the context of research projects.

---
## Current infrastructures in brief

![w:700 center](fig/infras.png)

--- 
## Project management and support

* Requirements identification, consulting, expertise (HPC, data analysis and management)
* POC development
* Participation in scientific projects
* Documentation
* Co-supervision (students, non-permanent technicians or engineers)

---
## What can we cover?

* Scientific computing: technical choices (hard and soft), parallelization, optimisation, numerical methods…
* Research software development: technical choices, good pratices (versioning, CI/CD, documentation), licensing, diffusion, software reproducibility 
* Research data management: data storage, data collection, treatment and analysis, archiving, distribution, DB, regulation(s), security, DMP,…

---

![w:700 center](fig/cdga.png)

---
## CDGA: an operational structure (1/2)

* GRICAD members drives this structure which aims to answer **concretely** to all the scientific questions about data
* Provide an **unique entrypoint** for all research communities
* To set up and lead a **network of data referents** for each laboratory

---
## CDGA: an operational structure (2/2)

* To set up **tools, services and infrastructures** that meet the needs of communities
* **To advise, animate and train** research staff on all aspects of data management (including sensitive data)
* To monitor **legal and technical** developments around data management and to participate in national, European and international initiatives
* Composition: 
  * 16 members, from various institutes
  * Technical, Open Science and legal skills.
  * To contact the CDGA: uga-cellule-data@univ-grenoble-alpes.fr
  * Website: https://gricad.gricad-pages.univ-grenoble-alpes.fr/cellule-data-stewardship/web/

---
## To obtain help and contact us

- GRICAD website: https://gricad.univ-grenoble-alpes.fr
- Documentation: https://gricad-doc.univ-grenoble-alpes.fr
- To contact us: gricad-contact@univ-grenoble-alpes.fr
- Need support? Our helpdesk: sos-gricad@univ-grenoble-alpes.fr
- Punctual question, discussions: https://gricad.slack.com

---
# Thank you for your attention! 

## Questions/Remarks are welcomed

---
# How do we finance infrastuctures

* Participation in funded projects (regional, national, european - EOSC)
* Financial support from UGA, G-INP, CNRS and INRIA
* And, more importantly, financial participation from scientific projects
* **Difficulties** : no sustainability, a lot of time and energy spent finding funding

---
## Involvement in the Open Science

* Open Science
  * Organize support for researchers on issues related to Open Science
    * Data management, storage, distribution, legal issues, archiving,...
    * Software reproducibility
  * ...through tools...
    * data repository, data mining platform, cloud storage 
    * Software forge, GUIX, JupyterHub, BinderHub
  * And training/animation!

---
## Reducing our environmental impact

* Carbon footprint of our infrastructures and their uses: DONE
* At our level, several initiatives to reduce our own impact (as an organization)
* Training to raise awareness among scientists about good practices of: 
  * Software develoment
  * Scientific computing
  * Data management

---
## European, national and local level

![w:700 center](fig/comp_pyramid.png)