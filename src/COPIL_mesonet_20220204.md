---
marp: true
theme: gricad
paginate: true
footer: "COPIL Mesonet - *04/02/2022*"
---
# PGD e-infrastructures (1/2)

Contexte : 

* Document exigé par l'ANR
* PGD **e-infrastructures** (≠ PGD données recherche)
* Pas vraiment de précurseur
* V1 produite par Violaine Louvet, avec relecture de J-P Roux, A. Arnaud, J. Pansanel et P-A Bouttier

---
# PGD e-infrastructures (1/2)

Attendus : 

* D'ici le prochain COPIL (ou AG ?), relecture par les membres du COPIL avec questions/remarques/suggestions
* Liens avec Portail et Suppport


---
# Audit de sécurité

Pour les partenaires hébergeur, du financement était prévu pour des audits de sécurité. 

Pour AURA (UGA partenaire hébergeur), nous aimerions lancer un audit (sur les deux sur lesquels nous nous étions engagés) avant l'arrivée des machines. 

- 280 k€ pour 13 audits prévus, 21500 € par audit.
- Pour le financement : faut-il un devis, une facture acquittée, ou juste prévenir ?
- Prestataires connus ?
- Retour Arnaud pour Reims ?