---
marp: true
theme: gricad
paginate: true
footer: "Atelier 1 #FLOSS_ESR - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---

# Pratiques générales pour la reproductibilité logicielle 

*Pierre-Antoine Bouttier, UAR GRICAD, CNRS*

![w:500 center](fig/logo.png)

---

## D'où parle-je ? 

* Ingénieur de recherche, expert en calcul scientifique, (anciennement ?) spécialisé en mathématiques appliquées, **responsable d'équipe support/accompagnement**...

* ...travaillant à l'UAR GRICAD, basée à Grenoble, fournissant **services, infrastructures et expertise** en soutien à toutes les communautés de recherche grenobloises autour du **calcul scientifique**, du **développement logiciel** et de la **gestion des données de la recherche**.  
* Je ne suis pas...
  * ...Un chercheur en informatique
  * ...Quelqu'un impliqué depuis longtemps dans la recherche reproductible

---

# Quelques situations rencontrées au quotidien

* Statistiques sur une enquête
* Nettoyage et pré-traitement de données brutes de mesures
* Simulations numériques
* Analyse et visualisation de jeux de données
* ...

* **Points communs** : **ordinateur**, **données** & **logiciel(s)**

--- 
# Les expérimentations numériques

![w:900 center](./fig/num.png)

---

# Pourquoi être reproductible ? 

**Reproductibilité** : une preuve de rigueur qui inspire confiance

* Ce qu'un résultat non-reproductible suggère : 
  * Une description incomplète de la méthodologie
  * Une maîtrise approximative des techniques utilisées
  * Une erreur ou, pire, une fraude

* L'importance de la confiance
  * Être à l'aise dans son propre travail
  * Produire des connaissances robustes et fécondes
  * Produire des connaissances dont l'ensemble de la société peut s'emparer

---
# Trois remarques générales

* Différents types de reproductibilité
* Reproductibilité : Une démarche pour prouver notre rigueur...
* ...à travers des outils et pratiques **transparents**, **lisibles** et **accessibles** 

---
# Les expérimentations numériques - Programme

![w:900 center](./fig/num2.png)

---
# Les 3 piliers

* Open source et Logiciel libre
  * Exigence de transparence
  * Seul moyen de garantir la reproductibilité dans le temps et l'espace
* Documentation de la méthodologie
  * Exigence de lisibilité
  * Au sens large : documentation du programme, code explicite, notebooks, site web, etc.
* Forge logicielle (e.g. gitlab institutionnel) 
  * Exigence d'accessibilité...
  * ...du programme et de la documentation
  * Software Heritage
  
---

# Les expérimentations numériques - L'environnement logiciel

![w:900 center](./fig/num3.png)

---
# Quelques pistes

* Préservation des 3 piliers dans l'environnement logiciel
* Utilisation si possible de solutions faites pour la reproductibilité : **Guix** et/ou **Nix**

---

# Merci de votre attention