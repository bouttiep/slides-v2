---
marp: true
theme: gricad
paginate: true
footer: "Aspects énergétiques - 29/11/2022 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---

# Aspects énergétiques du calcul scientifique et de la gestion des données
## Problématiques GRICAD à court et moyen terme 

*Pierre-Antoine Bouttier, UAR GRICAD, CNRS*


![w:500 center](fig/logo.png)

---
## Un peu de contexte

- Tensions prévues sur le réseau électrique cet hiver (et les suivants ?)
- Coût de l'énergie croissant 
- Conscience croissante de l'impact environnemental des services numériques pour la recherche

* Des éléments contextuels qui se recoupent et qui agissent à différentes échelles temporelles et impactent les services GRICAD

---
## Un peu de contexte

- **Tensions prévues sur le réseau électrique cet hiver (et les suivants ?)**
- Coût de l'énergie croissant 
- Conscience croissante de l'impact environnemental des services numériques pour la recherche

---
## Délestages envisagés

- Ce qui est aujourd'hui annoncé (par la présidence UGA) : 
  - Délestage à l'UGA en dernier recours
  - Coupure limitée à 2h
  - Usagers potentiels prévenus plusieurs jours à l'avance
  - Usagers réellement impactés prévenus 24h à l'avance

---
## Organisation GRICAD (dans ce contexte)

- Nous préviendrons les utilisateurs dès que nous aurons connaissance d'un délestage potentiel
- Si concernés par le délestage, nous prévoierons un arrêt des infrastructures un peu avant le délestage
  - Côté utilisateur, perte potentiel de jobs sur les supercalculateurs (si walltime>24h)
  - Services (jupyterhub, VMs et données) seraient inaccessibles mais pas de pertes à prévoir. 
  - Redémarrages annoncés et reprise automatique des jobs.
  - **Possibles évolutions du contexte et donc de nos process. Nous vous tiendrons informés** 

---
## Côté utilisateurs

* Questions : 
  * Envisagez-vous des actions particulières pour faire face à de possibles délestages ? 
  * Quels seraient les impacts d'interruptions de service trop fréquentes selon vous ?

---
## Un peu de contexte

- **Tensions prévues sur le réseau électrique cet hiver (et les suivants ?)**
- **Coût de l'énergie croissant**
- Conscience croissante de l'impact environnemental des services numériques pour la recherche

--- 
## Objectifs de l'UGA

* Réduction de la consommation d'énergie de 10% entre 2019 et 2024
* Quelques chiffres (pour 2019) : 
  * 59GWh.an pour le chauffage
  * 30GWh.an pour l'électricité
  * 1,7GWh.an pour DC IMAG (environ 500MWh.an pour HPC)
* Economie de 12GWh.an à réaliser (taille du parc estimée 2024>2019)

---
## Efforts de GRICAD

* Conscience de l'enjeu énergétique depuis le début de GRICAD mais aussi bien avant (e.g. Froggy)
* Participation à l'effort (à la marge, mais pas négligeable) important pour nous mais aussi pour nos tutelles
  * Arrêt de froggy (fin de vie)
  * Arrêt des infrastructures aux périodes de fermetures administratives de l'UGA ? Votre avis ? 
  * Sobriété forcée des usages ?

---
## Un peu de contexte

- **Tensions prévues sur le réseau électrique cet hiver (et les suivants ?)**
- **Coût de l'énergie croissant**
- **Conscience croissante de l'impact environnemental des services numériques pour la recherche**

--- 
## Piloter la surface des ressources vs. Se restreindre sur leurs usages

* Diminuer la fréquence des processeurs, arrêter une partie des serveurs, ne diminue pas la consommation énergétique sur une période suffisamment longue : pilotage, aux instants t, la surface de calcul
* Les besoins (stockage & calcul) augmentent plus rapidement que l'efficacité énergétique des serveurs (sans parler du coût environnemental de fabrication)

---
## Comment diminuer l'usage des infrastructures numériques ?...

...et donc leur dimensionnement et coût (environnemental et économique) de fonctionnement. 

En pratique : 
* Diminuer les volumes de données à stocker et/ou la durée de stockage
* Diminuer les calculs et les usages de serveurs. 

---
## Questions aux utilisateurs

- Est-ce que vous vous sentez concernés par ce sujet ? 
- Est-ce que dans votre communauté/structure/labo, des efforts concrets vont dans cette direction ? 

---
# Merci à tous