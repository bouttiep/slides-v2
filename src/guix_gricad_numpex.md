---
marp: true
theme: gricad
author: Pierre-Antoine Bouttier
paginate: true
footer: GUIX@NUMPEX - 04/04/2023  - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*
---
<!-- headingDivider: 2 -->

<!-- Pros
- On sait exactement ce qui est compilé (sécurité)
- Partage des paquets et liens symboliques 
- Simple UI -->

# GUIX@GRICAD
**Retour d'expérience d'un mésocentre de calcul**

Workshop NUMPEX - 04/04/2023
*Pierre-Antoine Bouttier*

# D'où je parle ?

* Ingénieur de recherche, expert en calcul scientifique, (anciennement ?) spécialisé en mathématiques appliquées, **responsable d'équipe support/accompagnement**...
* ...travaillant à l'UAR GRICAD, basée à Grenoble, fournissant **services, infrastructures et expertise** en soutien à toutes les communautés de recherche grenobloises autour du **calcul scientifique**, du **développement logiciel** et de la **gestion des données de la recherche**.  
* Je ne suis pas...
  * ...un ASR
  * ...un chercheur en informatique

# Notre contexte

<!--Faire le lien avec le snouveaux usages sur le continuum du calcul-->

* ~~4~~ 3 clusters de calcul en propre (un peu moins 5000 coeurs CPU, environ 100 GPUs)...
* ...reliés entre eux et à des clusters de labos par une grille de calcul local (*CiGri*)
* Utilisations : 
  *  HPC, HTC
  *  Traitement de données
  * Visu, formation & développement
* **Grande hétérogénéité** des usages, des communautés, des niveaux de compétences utilisateur
* **Le défi** : trouver un outil de gestion d'environnement logiciel qui réponde à des contraintes très hétérogènes

# LE contexte

* **La Science Ouverte** : Comment assurer la reproductibilité des *traitements numériques* ?
  * Bonnes pratiques de développement logiciel 
  * Et l'environnement logiciel ?  
* Changements de pratique
  * Utilisation croissantes des plateformes non-HPC : jupyterhub, binderhub, cloud computing...
  * ...dans la même chaîne de traitement
  * Développement de plateformes calcul/données distribuées

# The Good Ol' days

* Jusqu'à 2015, utilisation de module
  * Classique, bien connu 
  * Utilisé au tiers-1 et au tiers-2
  * Usages et communautés (beaucoup) plus homogènes
  
* Mais des gênes se font sentir : 
  * **Portabilité très moyenne** (indispensable pour notre petite grille !)
  * **Duplication des efforts** proportionnelle au nombre de clusters
  * **Aspect communautaire marginal** (*e.g.* pas de partage direct de paquets, binaires)
  * ...

# L'utopie logicielle que l'on cherchait

* **Isolation par rapport au système hôte**
* **Maintenance** (arbre des dépendances bien géré), **reproductibilité** (version unique d'un paquet - src, compilation, desc, déf,...- a la même sortie où que ce soit), **portabilité**
* **Complètement fonctionnel en espace utilisateur**
* **Workflow automatisé** : paquets *custom*, rebuilds automatiques, du PC perso aux clusters, CI
* **OS-indépendant**

* Installation de **Nix** en 2015 puis **GUIX** en 2018.

# L'installation de Guix

- Montages NFS `/gnu/store` et `/var/guix` sur tous les clusters
- Install "classique" depuis la doc de Guix, en root, sur un cluster (`luke`)
- Utilisateurs guixbuilder01 à guixbuilder10 et le groupe guixbuild sur `luke`
- Lancement du démon guix sur `luke` et écoute en tcp depuis les autres clusters.
- Un script à sourcer, côté utilisateur, sur chaque cluster à chaque connexion 

# GUIX, côté utilisateurs

* **Débutants** 
  * Pas de problèmes spécifiques, ils suivent la doc.
  * Mais des 
* **Historiques**
  * Adaptation par rapport à module (assez rapide)
  * Mais quelques soucis pour certains softs... (j'y reviendrai)
* **Power users**
  * Prise en main OK (et souvent enthousiaste)
  * Appropriations de l'outil (`guix shell`, `manifest.scm` & `guix describe`, etc.) 

# GUIX, côté support

# Les débuts

* Liberté des utilisateurs = moins de demandes d'installation d'environnement...
* ...mais demandes quand même ! 
* Mise au niveau un peu longue : 
  * Adapation au langage (DSL pour Nix, Guile pour Guix)
  * Gestion des logiciels propriétaires

# Et maintenant ? 

Après montée en compétence, plein de bénéfices et aucun regret !
  * Grande base de paquets existants
  * `guix shell`, `guix pack`, options de transformation de paquets...
  * Portabilité
  * Reproductibilité accentuée pour nos utilisateurs

# Cons 

* Demande de la ressource et de l'ingéniosité pour certaines suites logicielles
  * Logiciels propriétaires (e.g. Intel OneAPI)
  * Frameworks complexes (PyTorch, TensorFlow)
* Communauté HPC encore modeste (mais active et croissante)

# La communauté Guix-HPC

Regroupement d'utilisateurs ou mainteneurs de Guix pour le HPC...
  * Retour et partage d'expérience
  * Partage de paquets !
  * Animation et dissémination autour de l'utilisation de Guix et de ses possibilités 
  * Des besoins qui se transforment en feature !

# Conclusion 

* D'un point de vue purement *exploitation*, **nous n'avons aucune envie de changer d'outil !** 
* **GUIX** (ou Nix) reste **le meilleur outil** pour inscrire pleinement nos utilisateurs dans le cadre de **la Science Ouverte**
* **On peut aller encore plus loin dans la vie communautaire** autour de GUIX pour le rendre encore plus accessible

# Merci de votre attention
